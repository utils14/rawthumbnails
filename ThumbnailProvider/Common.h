#pragma once

#include <windows.h>
#include <shlobj.h>
#include <shlwapi.h>
#include <thumbcache.h>
#include <strsafe.h>

STDAPI_(ULONG) DllAddRef();
STDAPI_(ULONG) DllRelease();
STDAPI_(HINSTANCE) DllInstance();

// {68BF0019-79AB-4e7f-9500-AA3B5227DFE6}
#define szCLSID_SampleThumbnailProvider L"{68BF0019-79AB-4e7f-9500-AA3B5227DFE6}"
DEFINE_GUID(CLSID_SampleThumbnailProvider, 0x68bf0019, 0x79ab, 0x4e7f, 0x95, 0x0, 0xaa, 0x3b, 0x52, 0x27, 0xdf, 0xe6);

