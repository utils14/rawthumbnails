# Raw Thumbnails

This tools enables windows OS to generate preview thumbnails for RAW files.

## Installation

This project includes [Inno-Setup](https://jrsoftware.org/isinfo.php) based installation script


## Usage

After runnig the installation, all *.raw files will recieve preview thumbnails.
This thumbnails assume, each pixel value is:
    *16 bit unsigned integer
    *Little endian

If your system has a different raw files (depends on the HW generates this raw files),
You will need to change this in the source code.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)
